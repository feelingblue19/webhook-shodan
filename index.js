const express = require('express')

const app = express()
app.use(express.json({limit: '50mb'}))
app.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit:50000 }));

const port = 3000

app.listen(port, function () {
    console.log(`Service listening on port ${port}!`)
})

app.get('/', function (req, res) {
    res.json({
        'status': 'Service 1 is up and running'
    })
})

app.post('/webhook', function (req, res) {
  let body = req.body
  let header = req.headers

  console.log(body)
  console.log(header)

  res.json({
    'status': 'OK'
  })
})
